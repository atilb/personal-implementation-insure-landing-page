let isOverlayOn = false;
let hamburgerMenu = null;
let overlayMenu = null;
let contentPage = null;

window.addEventListener("load", function(event) {
  hamburgerMenu = document.getElementById("hamburger-menu");
  overlayMenu = document.getElementById("overlay-menu-mobile");
  hamburgerMenu.addEventListener("click", ChangeOverlayDisplay);
});

function ChangeOverlayDisplay()
{
  if(isOverlayOn)
  {
    isOverlayOn = false;
    overlayMenu.style.width = "0%";
    hamburgerMenu.src = "images/icon-hamburger.svg";
  }
  else{
    isOverlayOn = true;
    overlayMenu.style.width = "100%";
    hamburgerMenu.src = "images/icon-close.svg";
  }
}
